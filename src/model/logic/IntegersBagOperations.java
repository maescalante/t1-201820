package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getMin (IntegersBag bag) {
		int min= Integer.MAX_VALUE;
		int value;
		if (bag!=null) {
			Iterator<Integer> iter=bag.getIterator();
			while (iter.hasNext()) {
				value=iter.next();
				if (value<min) {
					min=value;
				}
			}
		}
		return min;
	}
	
	public boolean isThere(IntegersBag bag, int num) {
		boolean ans=false;
		int value;
		if (bag!=null) {
			Iterator<Integer> iter=bag.getIterator();
			while (iter.hasNext() && ans==false) {
				value=iter.next();
				if (value==num) {
					ans=true;


				}
			}
		
	}
		return ans;
}
	
	public int even(IntegersBag bag) {
		int value;
		int num=0;
		if (bag!=null) {
			Iterator<Integer> iter=bag.getIterator();
			while (iter.hasNext()) {
				value=iter.next();
				if (value%2==0) {
					num++;
				}
			}
		}
		return num;
	}
}
