package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag bag){
		return model.getMax(bag);
	}
	public static double getMin(IntegersBag bag){
		return model.getMin(bag);
	}
	
	public static boolean isThere(IntegersBag bag, int num){
		return model.isThere(bag, num);
	}
	public static double even(IntegersBag bag){
		return model.even(bag);
	}
}
